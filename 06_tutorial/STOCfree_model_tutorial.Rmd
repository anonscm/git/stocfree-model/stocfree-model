---
title: "STOC free model tutorial"
author: "Aurélien Madouasse"
date: "13/06/2019"
output:
  word_document:
  toc: TRUE
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# About this file

This file is a tutorial on how to use the STOC free model for the prediction of a probability of infection from data on regular test results and risk factors.

A small dataset is used for the demonstration.

This file was generated with R and RStudio in markdown.

# Required software

To run the model, the following programmes must be installed :

- R : for data manipulation
- JAGS : programme for Bayesian inference

In R, the following packages must be installed:

- *tidyverse* : this is a collection of packages that allow writing the R code in a more readable and efficient way
- *rjags* : allows running JAGS from R
- *tidybayes* : package used to process the results of the JAGS model



```{r, message=FALSE}
library(tidyverse)
library(rjags)
library(tidybayes)
```

# Data

A small toy dataset is used. The data are loaded :

```{r}
bvd <- as_tibble(
  read.csv("data/STOCfree_test_data.csv"))
```

The dataset has the following structure :

```{r}
bvd
```

The variables are :

- *id*: row number
- *herd*: herd identifier. Has to go from 1 to the number of herds
- *testTime*: time of test. Needs to be an integer

There are variables for 2 risk factors that were looked at in a preliminary analysis. Both variables are continuous. It would be possible to use discretised versions of these variables by creating dummy variables. 

Although this is unimportant for our purpose, these variables measure:

- *locPrev_p*: prevalence of seropositives in the municipality at the previous test
- *ln_nOrig6_12*: natural logarithm of the number of origins +1 of the animals pruchased in the period going from 6 months to 12 months before the test.


# Modeling strategy

The aim is, for each herd, to predict the status regarding infection at the last test of the series.

For example, for herd 1, we have data for 11 tests. The data for the first 10 tests will be used to estimate infection dynamics as well as the association between risk factors and the probability of new infection.

The probability of infection at test 11 will be predicted from:

- the probability of infection at test 10, which itself depends on the probability on infection on the previous tests
- the known risk factors at test 11. These could be calculated at any time between test 10 and test 11.

# JAGS model

## Model code

This is the JAGS code for the model.

```{r}
cat("model{
    
  for(h in 1:nHerds){

   # First test result
   pi[indI[h]] ~ dbeta(pi1_beta_a, pi1_beta_b) 
   Status[indI[h]] ~ dbern(pi[indI[h]])

   pTestPos[indI[h]] <-  Se * Status[indI[h]] + (1 - Sp) * (1 - Status[indI[h]])
    testRes[indI[h]] ~ dbern(pTestPos[indI[h]])

   
   # Tests 2 to 1 minus final
   for(t in indJ[h]:indF[h]){

    logit(tau1[t]) <- theta[1] + theta[2] * locPrev_p[t] + theta[3] * ln_nOrig6_12[t]
    
    pi[t] <- tau1[t] * (1 - Status[t-1]) +
             tau2 * Status[t-1]
             
    Status[t] ~ dbern(pi[t])
    
    pTestPos[t] <-  Se * Status[t] + (1 - Sp) * (1 - Status[t])
    testRes[t] ~ dbern(pTestPos[t])

      } #t
      
    # Statuses to predict

    ## After having observed the risk factors
    logit(tau1Pred[h]) <- theta[1] + theta[2] * locPrev_p[indP[h]] + theta[3] * ln_nOrig6_12[indP[h]]
    
    piPred[h] <- tau1Pred[h] * (1 - Status[indP[h] - 1]) +
             tau2 * Status[indP[h] - 1]

    
    } #h

  ## Priors
 
  ## Priors for sensitivities and specificities
    Se ~ dbeta(Se_beta_a, Se_beta_b)
    Sp ~ dbeta(Sp_beta_a, Sp_beta_b)
    
  ## Probability of not eliminating the infection
    tau2 ~ dbeta(tau2_beta_a, tau2_beta_b)
    
  ## Logistic regression coefficients
    theta[1] ~ dnorm(theta1_norm_mean, theta1_norm_prec)
    theta[2] ~ dnorm(theta2_norm_mean, theta2_norm_prec)
    theta[3] ~ dnorm(theta3_norm_mean, theta3_norm_prec)
    
    }", file =  "STOCfree_modelFile.txt")
```

## Model priors

Strong constraints need to be put on the parameters because there are not much data to learn from. The priors put on the parameters will be either based on hypotheses or on the results of preliminary analyses. This preliminary analysis should have been carried out on a different dataset.


### Prevalence of infection at the first test

For the first test in each herd, there are no previous data on either status or test results. A prior needs to be put on the probability of infection. A Beta distribution with the following parameters is used.

```{r}
pi1_beta_a <- 1
pi1_beta_b <- 2
```

This distribution looks as follows:

```{r}
curve(dbeta(x, pi1_beta_a, pi1_beta_b),
      xlab = "Probability of infection on the first test",
      ylab = "Density")
```

### Probability for infected herds of not eliminating the infection

From the preliminary analysis, we know that this above 0.85.

```{r}
tau2_beta_a <- 30
tau2_beta_b <- 2
```

```{r}
curve(dbeta(x, tau2_beta_a, tau2_beta_b),
      xlab = "tau2",
      ylab = "Density")
```

### Sensitivity of BTM ELISA for the detection of infected herds

We assume that there may be some false negatives. The following Beta distribution is used:

```{r}
Se_beta_a <- 12
Se_beta_b <- 2
```


```{r}
curve(dbeta(x, Se_beta_a, Se_beta_b),
      xlab = "Sensitivity",
      ylab = "Density")
```

### Specificty of BTM ELISA for the detection of infected herds

The specificity is assumed to be excellent.

```{r}
Sp_beta_a <- 200
Sp_beta_b <- 4
```

```{r}
curve(dbeta(x, Sp_beta_a, Sp_beta_b),
      xlab = "Specificity",
      ylab = "Density")
```

### Association between risk factors and the probability of new infection

These are the coefficients of a logistic model of the probability of new infection. 

The model intercept represents the log odds of new infection in herds that do not purchase any cattle, in a municipality with a seroprevalence of 0. In the preliminary analysis, the distribution of this parameter looked as follows:

```{r}
theta1_norm_mean <- -3.8
theta1_norm_sd <- .15

curve(dnorm(x, theta1_norm_mean, theta1_norm_sd), 
      from = -4.3, to = -3.3,
      xlab = "theta.1", ylab = "Density")
```

This can be represented on the probability scale by using the inverse logit function.

```{r}
invlogit <- function(x) exp(x) / (1 + exp(x) )
```

Here is a plot for the prior probability of infection in the reference category, i.e. in herds that did not purchase any animal in the 6 to 12 preceding months in a municipality with a seroprevalence of 0.

```{r}
plot(
  density(
    invlogit(
      rnorm(100000, theta1_norm_mean, theta1_norm_sd))),
  xlim = c(0, .1),
  main = "Prior distribution for the model intercept\non the probability scale",
  xlab = "Probability")
```

```{r}
theta2_norm_mean <- -.6
theta2_norm_sd <- 1.2

curve(dnorm(x, theta2_norm_mean, theta2_norm_sd), 
      from = -4, to = 3,
      xlab = "theta.2", ylab = "Density")
```

```{r}
theta3_norm_mean <- .6
theta3_norm_sd <- .1

curve(dnorm(x, theta3_norm_mean, theta3_norm_sd), 
      from = .2, to = 1,
      xlab = "theta.3", ylab = "Density")
```

## Model data

### Herd level data

For each herd, the row numbers need to be identified for:

- the first test (*indI* in the JAGS code)
- the second test (*indJ* in the JAGS code)
- the test before last (*indF* in the JAGS code)
- the last test (*indP* in the JAGS code), for which the probability of infection will be predicted 

Although *indJ* and *indF* could be calculated in JAGS, they are supplied as data to avoid uncessary calculation during the JAGS run.

This is done as follows:

```{r}
herd <- bvd %>% 
  group_by(herd) %>% 
  summarise(
    indI = id[testTime ==  min(testTime)],
    indJ = id[testTime ==  min(testTime)] + 1,
    indF = id[testTime ==  max(testTime)] - 1,
    indP = id[testTime ==  max(testTime)])
```

### Formatting data for JAGS

Creation of the dataset used by JAGS for inference and prediction.

```{r}
JAGS_data <- list(
  nHerds = max(herd$herd),
  indI = herd$indI,
  indJ = herd$indJ,
  indF = herd$indF,
  indP = herd$indP,
  testRes = bvd$testRes,
  locPrev_p = bvd$locPrev_p,
  ln_nOrig6_12 = bvd$ln_nOrig6_12,
  pi1_beta_a = pi1_beta_a,     
  pi1_beta_b = pi1_beta_b,
  tau2_beta_a = tau2_beta_a,
  tau2_beta_b = tau2_beta_b,
  Se_beta_a = Se_beta_a,
  Se_beta_b = Se_beta_b,
  Sp_beta_a = Sp_beta_a,
  Sp_beta_b = Sp_beta_b,
  theta1_norm_mean = theta1_norm_mean,
  theta1_norm_prec = 1 / theta1_norm_sd^2,
  theta2_norm_mean = theta2_norm_mean,
  theta2_norm_prec = 1 / theta2_norm_sd^2,
  theta3_norm_mean = theta3_norm_mean,
  theta3_norm_prec = 1 / theta3_norm_sd^2
  )
```

## Running JAGS

There are 3 steps in the process

- Compilation: the model needs to be compiled
- Burnin: This consists in model iterations that need to performed in order for the algorithm to converge to the posterior distribution. There is no absolute rule to determine the number of iterations to perform.
- Sampling: samples are drawn from the posterior distributions of all the parameters of interest.


### Model compilation

The model presented above is compiled. Four chains are run. The reason for running multiples chains is to assess convergence. The distribution of each parameter should be similar accross chains. This can only be assessed when there are multiple chains.

```{r}
JAGS_model <- jags.model( file = "STOCfree_modelFile.txt", 
                         data = JAGS_data,
                         n.chains = 4)
```

### Burnin

The model is run for a certain number of iterations so that the Markov chains can converge to the parameter posterior distributions. This phase could be performed with the next one and convergence assessed at this next stage. But, in the sampling phase, parameter draws are stored in memory. Given the important number of parameters monitored (notably the predicted probabilities of infection), the 2 steps are kept separated.

Because the aim of this document is to test the code, only 100 iterations of burnin are performed.

```{r}
update(JAGS_model, n.iter = 100)
```

### Sampling

Here we list the parameters we want to monitor

```{r}
savedParam <- c("Se", "Sp", "theta", "tau2", "piPred")
```

Here we call JAGS, which will draw samples from the posterior distributions of the model parameters and save the variables listed in *savedParam*.

*n.iter* is the number of iterations that will be performed. Increasing the number of iterations will increase the time the programme runs (which can be extremely long).

1 in *thin* iterations will be kept for later analysis. When thin equals 1, all the iterations will be kept, when thin equals 100, 1 iteration in a hundred will be kept. This is a way to avoid autocorrelation in the posterior draws.

```{r}
JAGS_samples <- coda.samples(JAGS_model,
                            variable.names = savedParam,
                            n.iter = 1000,
                            thin = 5)
```

## Results

The results of the JAGS run are stored in the *JAGS_samples* variable. I use the *tidybayse* package to store these data in a more user friendly way.

This is done for the model parameters.

```{r}
model_param <- spread_draws(JAGS_samples, Se, Sp, tau2, theta[..])
```

Then for the predicted probabilities of infection

```{r}
infProba <- spread_draws(JAGS_samples, piPred[herd])
```

These datasets should be stored as text files in order not to have to re run the simulations, which is time consuming.


### Assessing convergence

This is done visually. There more formal convergence diagnostic (r-hat).

```{r}
ggplot(model_param, aes(x = .iteration, y = Se, col = factor(.chain))) +
         geom_line()
```

### Parameter distributions

Below are densities for both the prior and posterior distributions of each parameter of interest.

```{r}
par(mfrow = c(1, 2), mar = c(3, 3, 3, 1))
plot(density(model_param$Se),
     xlim = c(0, 1),
     col = "red",
     main = "Sensitivity",
     xlab = "Sensitivity")
curve(dbeta(x, Se_beta_a,  Se_beta_b),
      add = TRUE,
      col = "grey", lwd = 2)
legend("topleft",
       c("Prior", "Posterior"),
       col = c("grey", "red"),
       lty = 1,
       bty = "n")

plot(density(model_param$Sp),
     xlim = c(0, 1),
     col = "red",
     main = "Specificity",
     xlab = "Specificity")
curve(dbeta(x, Sp_beta_a,  Sp_beta_b),
      add = TRUE,
      col = "grey", lwd = 2)
```

```{r}
par(mfrow = c(2, 2), mar = c(3, 3, 3, 1))
plot(density(model_param$tau2),
     xlim = c(0, 1),
     col = "red",
     main = "tau2",
     xlab = "tau2")
curve(dbeta(x, tau2_beta_a,  tau2_beta_b),
      add = TRUE,
      col = "grey", lwd = 2)
legend("topleft",
       c("Prior", "Posterior"),
       col = c("grey", "red"),
       lty = 1,
       bty = "n")

plot(density(model_param$theta.1),
     xlim = c(-4.5, -3),
     col = "red",
     main = "theta 1",
     xlab = "theta 1")
curve(dnorm(x, theta1_norm_mean,  theta1_norm_sd),
      add = TRUE,
      col = "grey", lwd = 2)

plot(density(model_param$theta.2),
     xlim = c(-4, 2),
     col = "red",
     main = "theta 2",
     xlab = "theta 2")
curve(dnorm(x, theta2_norm_mean,  theta2_norm_sd),
      add = TRUE,
      col = "grey", lwd = 2)

plot(density(model_param$theta.3),
     xlim = c(0, 2),
     col = "red",
     main = "theta 3",
     xlab = "theta 3")
curve(dnorm(x, theta3_norm_mean,  theta3_norm_sd),
      add = TRUE,
      col = "grey", lwd = 2)
```

### Predicted probabilities of infection

Below are the disitributions of the probability of infection for each of the 100 herds for which we have data

```{r}
infProba %>% 
  ggplot(aes(x = piPred, col = factor(herd))) +
  geom_density()+ 
  theme(legend.position = "none") +
  xlab("Probability of infection")
```

Herd probabilities can be examined individually. This is illustrated for 3 herds.

```{r}
infProba %>% 
  filter(herd %in% c(9, 46, 81)) %>% 
  ggplot(aes(x = piPred, col = factor(herd))) +
  geom_density()+ 
  theme(legend.position = "none") +
  xlab("Probability of infection")
```

We then calculate summary statistics for each herd.

```{r}
herdPred <- infProba %>% 
  group_by(herd) %>% 
  summarise(
    med_pred = median(piPred),
    mean_pred = mean(piPred),
    pctl2.5_pred = quantile(piPred, .025),
    pctl97.5_pred = quantile(piPred, .975)
  ) %>% 
  mutate(diff_LH = pctl97.5_pred - pctl2.5_pred)
```

The distribution of the median probability of infection per herd is plotted.

```{r}
ggplot(herdPred, aes(x = med_pred)) +
  geom_density()
```




