---
title: "Data_management_sondages_lait_BVD"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Remarques générales

- Créer une hiérarchie dans le document avec des #. # Titre 1 ## Titre 2 ### Titre 3 ...
- Eviter de charger trop de packages. Quand c'est possible, utiliser des packages assez utilisés. En général, on sait qu'ils seront maintenus longtemps et il est plus facile de trouver de l'aide sur internet.
- Eviter les points et les espaces dans les noms de variables quand c'est possible. Préférer les underscores si besoin.
- Ne pas hésiter à expliquer le code, en montrant à quoi ressemble le jeu de données et en expliquant ce qui est fait.


# Packages

J'ai créé une nouvelle version de ce chunk plus bas dans laquelle j'ai inclus les packages au fur et à mesure que j'en ai besoin.

```{r}
library(tidyverse)
library(readxl)
```


# Importation des données de sondage du lait des différentes feuilles excel

Toutes les données sont importées avec la fonction read_excel du package readxl. Cela évite de charger trop de packages. Rend le code plus lisible.

Il reste des erreurs au niveau de l'impor des dates. A vérifier.

```{r, warning=FALSE}
Automne_2014     <- read_excel("sondages lait/SondageBvdAutomne2014.xls",
                               sheet=1)
Printemps_2015   <- read_excel("sondages lait/Printemps2015.xlsx",
                               sheet = 1)
Automne_2015     <- read_excel("sondages lait/ToutAutomne2015.xls", 
                               sheet = 1, skip = 1)
Printemps_2016   <- read_excel("sondages lait/ToutPrintemps2016.xlsx", 
                                sheet = 1, skip = 1)
Automne_2016     <- read_excel("sondages lait/ToutAutomne2016.xlsx", 
                               sheet = 1, skip = 1)
Printemps_2017   <- read_excel("sondages lait/GlobalPrintemps2017.xlsx", 
                               sheet = 1, skip = 1)
Printemps_2017_1 <- read_excel("sondages lait/GlobalPrintemps2017.xlsx", 
                               sheet = 2, skip = 1)
Automne_2017     <- read_excel("sondages lait/recap.xlsx", 
                               sheet = 1, skip = 1)
Printemps_2018   <- read_excel("sondages lait/RecapTotal04062018.xlsx", 
                               sheet = 1, skip = 1)
Automne_2018     <- read_excel("sondages lait/Liasse15_10_18.xlsx", 
                               sheet = 1, skip = 1)
Printemps_2019   <- read_excel("sondages lait/Liasse08_03_19.xlsx", 
                               sheet = 1, skip = 1)
```


La liste des campagnes est la suivante:

```{r}
CAMPAGNES <- tibble(
  Cmp = paste(c("PRINTEMPS", "AUTOMNE"), rep(2002:2019, each = 2)),
  nCmp = NA)

CAMPAGNES <- CAMPAGNES[-nrow(CAMPAGNES),]
CAMPAGNES$nCmp <- 1:nrow(CAMPAGNES)
```


## Avant 2015

Toutes les données sont contenus dans la tabme *Automne_2014*.

Les noms des colonnes qui sont gardées à ce stade sont

```{r, warning=FALSE}
Automne_2014     <- read_excel("sondages lait/SondageBvdAutomne2014.xls",
                               sheet=1)
```


```{r}
cln <- colnames(Automne_2014)[-c(21,23,27,28,29,33,34,35,39,40,41,45,46,47,51,52,53,54,58,59,63,64,65,66,67,71,72,73,74,78,79,80,81,85,86,87,91,92,93,97,98,99,100,104,105,106,107,111,112,113,114,118,119,120,121,125,126,127,128,129,130,131,132,133,134,135,136,137:144,147:149,151:186)]

cln
```


```{r}
Automne_2014 <- Automne_2014[, cln]
```

Ces colonnes sont renommées comme suit :

```{r}
Automne_2014 <- Automne_2014 %>%
  rename(EDE = "N° EDE")

colnames(Automne_2014) <- str_replace(colnames(Automne_2014), "%\n", "TAUX_")
colnames(Automne_2014) <- str_replace(colnames(Automne_2014), "RES BVD\n", "NOTE_")
colnames(Automne_2014) <- str_replace(colnames(Automne_2014), "RES\n", "NOTE_")
colnames(Automne_2014) <- str_replace(colnames(Automne_2014), "date\nanal\n", "DATE_")
colnames(Automne_2014) <- str_replace(colnames(Automne_2014), "date ana \n", "DATE_")
colnames(Automne_2014) <- str_replace(colnames(Automne_2014), "date ana ", "DATE_")
```

On regarde les contenus de chaque type de colonne. Colonnes *TAUX*

```{r}
cln <- grep("TAUX_", colnames(Automne_2014))
for(i in cln) Automne_2014[,i] <- sapply(Automne_2014[,i], as.double)

Automne_2014 %>%
  select(grep("TAUX_", colnames(Automne_2014)))
```

Colonnes *NOTE*


```{r}
cln <- grep("NOTE_", colnames(Automne_2014))
for(i in cln) Automne_2014[,i] <- sapply(Automne_2014[,i], as.integer)

Automne_2014 %>%
  select(grep("NOTE_", colnames(Automne_2014))) %>%
  View()
```

Colonnes *DATE*

```{r, eval = FALSE}
Automne_2014 %>%
  select(grep("DATE_", colnames(Automne_2014))) %>%
  View()
```

Il y a un problème avec les colonnes *DATE_09/09*, *DATE_02/10* et *DATE_09/10*. Du fait de la présence de caractères spéciaux, les dates ont été converties au format numérique. Il est possible de retrouver les dates d'origines avec as.Date().


```{r}
cln <- match(c("DATE_09/09", "DATE_02/10", "DATE_09/10"), colnames(Automne_2014))

for(i in cln) Automne_2014[,i] <- sapply(Automne_2014[,i], function(x) as.character(as.Date(as.numeric(x), origin= "1900-01-01")))
```

Transformation au format long. C'est long !

```{r}
## 01-2002
BVD44 <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_01/02'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_01/02'),
                 DATE = rep(NA),
                 nCmp = rep(1))
## 09-2002
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_09/02'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_09/02'),
                 DATE = rep(NA),
                 nCmp = rep(2)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 02-2003
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_02/03'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_02/03'),
                 DATE = rep(NA),
                 nCmp = rep(3)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)


## 09-2003
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_09/03'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_09/03'),
                 DATE = rep(NA),
                 nCmp = rep(4)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 03-2004
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_03/2004'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_03/2004'),
                 DATE = rep(NA),
                 nCmp = rep(5)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 09-2004
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_09/2004'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_09/2004'),
                 DATE = rep(NA),
                 nCmp = rep(6)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 03-2005
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_03/05'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_03/05'),
                 DATE = rep(NA),
                 nCmp = rep(7)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 09-2005
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_09/05'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_09/05'),
                 DATE = rep(NA),
                 nCmp = rep(8)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 03-2006
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_03/06'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_03/06'),
                 DATE = rep(NA),
                 nCmp = rep(9)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 09-2006
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_09/06'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_09/06'),
                 DATE = rep(NA),
                 nCmp = rep(10)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 02-2007
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_02/07'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_02/07'),
                 DATE = as.Date(Automne_2014$'DATE_02/07', "%Y-%m-%d"),
                 nCmp = rep(11)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 09-2007
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_09/07'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_09/07'),
                 DATE = as.Date(Automne_2014$'DATE_09/07', "%Y-%m-%d"),
                 nCmp = rep(12)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 02-2008
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_02/08'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_02/08'),
                 DATE = as.Date(Automne_2014$'DATE_02/08', "%Y-%m-%d"),
                 nCmp = rep(13)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 09-2008
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_09/08'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_09/08'),
                 DATE = as.Date(Automne_2014$'DATE_09/08', "%Y-%m-%d"),
                 nCmp = rep(14)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 02-2009
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_02/09'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_02/09'),
                 DATE = as.Date(Automne_2014$'DATE_02/09', "%Y-%m-%d"),
                 nCmp = rep(15)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 09-2009
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_09/09'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_09/09'),
                 DATE = as.Date(Automne_2014$'DATE_09/09', "%Y-%m-%d"),
                 nCmp = rep(16)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 02-2010
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_02/10'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_02/10'),
                 DATE = as.Date(Automne_2014$'DATE_02/10', "%Y-%m-%d"),
                 nCmp = rep(17)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 09-2010
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_09/10'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_09/10'),
                 DATE = as.Date(Automne_2014$'DATE_09/10', "%Y-%m-%d"),
                 nCmp = rep(18)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 02-2011
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_02/11'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_02/11'),
                 DATE = as.Date(Automne_2014$'DATE_02/11', "%Y-%m-%d"),
                 nCmp = rep(19)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 10-2011
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_10/11'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_10/11'),
                 DATE = as.Date(Automne_2014$'DATE_10/11', "%Y-%m-%d"),
                 nCmp = rep(20)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 02-2012
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_02/12'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_02/12'),
                 DATE = as.Date(Automne_2014$'DATE_02/12', "%Y-%m-%d"),
                 nCmp = rep(21)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 09-2012
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_09/12'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_09/12'),
                 DATE = as.Date(Automne_2014$'DATE_09/12', "%Y-%m-%d"),
                 nCmp = rep(22)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 02-2013
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_02/13'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_02/13'),
                 DATE = as.Date(Automne_2014$'DATE_02/13', "%Y-%m-%d"),
                 nCmp = rep(23)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 10-2013
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_10/13'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_10/13'),
                 DATE = as.Date(Automne_2014$'DATE_10/13', "%Y-%m-%d"),
                 nCmp = rep(24)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 02-2014
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_02/14'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_02/14'),
                 DATE = as.Date(Automne_2014$'DATE_02/14', "%Y-%m-%d"),
                 nCmp = rep(25)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)

## 09-2014
tmp <- tibble(EDE = as.character(Automne_2014$EDE), 
                'TAUX' = as.numeric(Automne_2014$'TAUX_09/14'), 
                'NOTE' = as.numeric(Automne_2014$'NOTE_09/14'),
                 DATE = as.Date(Automne_2014$'DATE_09/14', "%Y-%m-%d"),
                 nCmp = rep(26)) %>%
  filter(!is.na(NOTE) | !is.na(TAUX))

BVD44 <- bind_rows(BVD44, tmp)
rm(tmp)



BVD44$CAMPAGNE <- CAMPAGNES$Cmp[match(BVD44$nCmp, CAMPAGNES$nCmp)]

BVD44 <- BVD44 %>%
  select(EDE, nCmp, CAMPAGNE, DATE, TAUX, NOTE)
```


## Printemps 2015

Il y a beaucoup de données dans cette table.

```{r, eval=FALSE}
colnames(Printemps_2015)
```

Les colonnes qui nous intéressent sont:

```{r}
Printemps_2015 <- Printemps_2015 %>%
  select('N° EDE', 'date ana \r\n01/02/2015', '%\r\n02/2015', 'Note 02/15') %>%
  rename(EDE = 'N° EDE', DATE = 'date ana \r\n01/02/2015', TAUX = '%\r\n02/2015', NOTE = 'Note 02/15') %>%
  mutate(EDE = as.character(EDE), 
         CAMPAGNE = rep("PRINTEMPS 2015"), 
         DATE = as.Date(DATE, "%d/%m/%Y"),
         NOTE = as.numeric(NOTE)) %>%
  select(CAMPAGNE, EDE, DATE, TAUX, NOTE)
```


## Table Automne 2015

```{r}
Automne_2015 <- Automne_2015 %>%
  select("CAMPAGNE", "NUMERO EDE", "DATE PRELEVEMENT", "TAUX", "NOTE") %>%
  rename(EDE = 'NUMERO EDE', DATE = 'DATE PRELEVEMENT') %>%
  mutate(EDE = as.character(EDE), DATE = as.Date(DATE, "%d/%m/%Y"),
         NOTE = as.numeric(NOTE))
```


## Table Printemps 2016

```{r}
Printemps_2016 <- Printemps_2016 %>%
  select("CAMPAGNE", "NUMERO EDE", "DATE PRELEVEMENT", "TAUX", "NOTE") %>%
  rename(EDE = 'NUMERO EDE', DATE = 'DATE PRELEVEMENT') %>%
  mutate(EDE = as.character(EDE), DATE = as.Date(DATE, "%d/%m/%Y"),
         NOTE = as.numeric(NOTE))
```


## Table Automne 2016

```{r}
Automne_2016 <- Automne_2016 %>%
  select("CAMPAGNE", "NUMERO EDE", "DATE PRELEVEMENT", "TAUX", "NOTE") %>%
  rename(EDE = 'NUMERO EDE', DATE = 'DATE PRELEVEMENT') %>%
  mutate(EDE = as.character(EDE), DATE = as.Date(DATE, "%d/%m/%Y"),
         NOTE = as.numeric(NOTE))
```


## Tables Printemps_2017

Il y a 2 tables pour le printemps 2017. Raison par entièrement claire mais possible que dû au fait que le labo avait décidé d'utiliser un nouveau test plus sensible sans en avertir le GDS. 

Sélection des variables d'intérêt dans les deux tables printemps 2017. La liste des colonnes est :

```{r}
colnames(Printemps_2017)
```

On sélectionne les 4 colonnes qui vont nous servir par las suite. Certaines de ces colonnes sont renommées pour supprimer les espaces et avoir des noms plus courts.

```{r}
Printemps_2017 <- Printemps_2017 %>%
  select(CAMPAGNE, 'NUMERO EDE', 'DATE PRELEVEMENT', TAUX, NOTE) %>%
  rename(EDE = 'NUMERO EDE', DATE = 'DATE PRELEVEMENT')

Printemps_2017_1 <- Printemps_2017_1 %>%
  select(CAMPAGNE, 'NUMERO EDE', 'DATE PRELEVEMENT', TAUX, NOTE) %>%
  rename(EDE = 'NUMERO EDE', DATE = 'DATE PRELEVEMENT')
```

On fusionne les 2 jeux de données puis on supprime Printemps_2017_1.

```{r}
Printemps_2017 <- bind_rows(Printemps_2017, Printemps_2017_1)

rm(Printemps_2017_1)
```

Les nuémros EDE sont mis au format caractères et la date de prélèvement est mise au format Date.

```{r}
Printemps_2017 <- Printemps_2017 %>%
  mutate(
    EDE = as.character(EDE),
    DATE = as.Date(DATE, "%d/%m/%Y"),
    NOTE = as.numeric(NOTE)) %>%
  arrange(EDE, DATE)
```

On supprime les doublons.

```{r}
Printemps_2017 <- Printemps_2017 %>% 
  distinct()
```

Il reste un élevage avec 2 lignes qui ne sont pas identiques.

```{r}
Printemps_2017 %>%
  group_by(factor(EDE)) %>%
  summarise(n = length(DATE)) %>%
  filter(n > 1)
```


## Table Automne 2017

```{r}
Automne_2017 <- Automne_2017 %>%
  select("CAMPAGNE", "NUMERO EDE", "DATE PRELEVEMENT", "TAUX", "NOTE") %>%
  rename(EDE = 'NUMERO EDE', DATE = 'DATE PRELEVEMENT') %>%
  mutate(EDE = as.character(EDE), DATE = as.Date(DATE, "%d/%m/%Y"),
         NOTE = as.numeric(NOTE))
```


## Table Printemps 2018

On fait comme pour la table prédédente. Ici, c'est plus simple car il n'y a qu'in seul fichier.

```{r}
Printemps_2018 <- Printemps_2018 %>%
  select("CAMPAGNE", "NUMERO EDE", "DATE PRELEVEMENT", "TAUX", "NOTE") %>%
  rename(EDE = 'NUMERO EDE', DATE = 'DATE PRELEVEMENT') %>%
  mutate(EDE = as.character(EDE), DATE = as.Date(DATE, "%d/%m/%Y"),
         NOTE = as.numeric(NOTE))
```

## Table Automne 2018

```{r}
Automne_2018 <- Automne_2018 %>%
  select("CAMPAGNE", "NUMERO EDE", "DATE PRELEVEMENT", "TAUX", "NOTE") %>%
  rename(EDE = 'NUMERO EDE', DATE = 'DATE PRELEVEMENT') %>%
  mutate(EDE = as.character(EDE), DATE = as.Date(DATE, "%d/%m/%Y"),
         NOTE = as.numeric(NOTE))
```

## Table Printemps 2019

```{r}
Printemps_2019 <- Printemps_2019 %>%
  select("CAMPAGNE", "NUMERO EDE", "DATE PRELEVEMENT", "TAUX", "NOTE") %>%
  rename(EDE = 'NUMERO EDE', DATE = 'DATE PRELEVEMENT') %>%
  mutate(EDE = as.character(EDE), DATE = as.Date(DATE, "%d/%m/%Y"),
         NOTE = as.numeric(NOTE))
```



## Fusion des différentes tables

La table *BVD44* contient les données du printemps 2002 à l'automne 2014.

```{r}
BVD44
```

Toutes les tables sont fusionnées.

```{r}
Printemps_2015 <- bind_rows(Printemps_2015,
                            Automne_2015,
                            Printemps_2016,
                            Automne_2016,
                            Printemps_2017,
                            Automne_2017,
                            Printemps_2018,
                            Automne_2018,
                            Printemps_2019)

Printemps_2015 <- Printemps_2015 %>%
  filter(!is.na(DATE) & !is.na(NOTE))
```

Les numéros de campagne sont aoutés

```{r}
BVD44$CAMPAGNE <- CAMPAGNES$Cmp[match(BVD44$nCmp, CAMPAGNES$nCmp)]

Printemps_2015$nCmp <- CAMPAGNES$nCmp[match(Printemps_2015$CAMPAGNE, CAMPAGNES$Cmp)]
```

Jeu de données final

```{r}
Printemps_2015 <- Printemps_2015 %>%
  select(EDE, nCmp, CAMPAGNE, DATE, TAUX, NOTE)

BVD44 <- bind_rows(BVD44, Printemps_2015) %>%
  arrange(nCmp, EDE)
```

Enregistrement de la base de données finale.

```{r}
write.csv2(BVD44, "BVD44.csv", row.names = FALSE, na="NA")
```

