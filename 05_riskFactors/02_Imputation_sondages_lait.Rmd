---
title: "Analyse_sondages_lait_BVD"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(dplyr)
library(questionr)
```


```{r}
sondages_lait_BVD <- read.csv2("sondages_lait_BVD.csv")
```

### IMPUTATION DONNEES MANQUANTES
- Cr�ation d'une table avec les �levages ainsi que leurs communes en enlevant les �levages ou il y a des donn�es manquantes
- Conversion des communes en charact�res
- x correspond � la liste de tous les num�ros d'identifications des �levages
- Boucle pour imputer les donn�es manquantes sur les communes, codes postaux ainsi que canton � partir des donn�es contenus dans la table cr�� pr�cedemment
```{r}
tabletest <- unique(sondages_lait_BVD[,1:4])
tabletest <- na.omit(tabletest)
tabletest$COMMUNE2 <- as.character(tabletest$COMMUNE2)
```

```{r}
x <- sondages_lait_BVD$NUMERO.EDE
for (i in 1:length(x)){
  if (is.na(sondages_lait_BVD$COMMUNE2[i])){
      sondages_lait_BVD$COMMUNE2[i] <- tabletest$COMMUNE2[sondages_lait_BVD$NUMERO.EDE==x[i]]
  }
}
```

```{r}
x <- sondages_lait_BVD$NUMERO.EDE
for (i in 1:length(x)){
  if (is.na(sondages_lait_BVD$CP2[i])){
      sondages_lait_BVD$CP2[i] <- tabletest$CP2[sondages_lait_BVD$NUMERO.EDE==x[i]]
  }
}
```

```{r}
x <- sondages_lait_BVD$NUMERO.EDE
for (i in 1:length(x)){
  if (is.na(sondages_lait_BVD$CANTON[i])){
      sondages_lait_BVD$CANTON[i] <- tabletest$CANTON[sondages_lait_BVD$NUMERO.EDE==x[i]]
  }
}
```
Il reste quelques donn�es manquantes sur les communes, codes postaux et canton mais ceci est du au fait qu'il y ait de nouveaux �levages qui �taient non r�pertori�s avant

```{r}
    write.csv2(sondages_lait_BVD, "sondages_lait_BVD_finale.csv", quote=FALSE, row.names = FALSE, na="NA")

```

