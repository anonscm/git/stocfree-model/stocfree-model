---
title: Analyse univariÃ©e bivariÃ©e et rÃ©gression sur les variables de la base de
  donnÃ©e Loire Atlantique
output:
  html_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(epiDisplay)
library(gdata)
library(sp)
library(rgdal)
library(RColorBrewer)
library(classInt)
library(ggmap)
library(maptools)
library(cartography)
library(yarrr)
library(questionr)
library(dplyr)
library(broom)
library(ggplot2)

```

## Analyse univariée
###Variables qualitatives : NUMERO.EDE, CP2, COMMUNE2, CANTON, CAMPAGNE, DATE.PRELEVEMENT, N.EDE.VENDEUR, achat, achat6mois, achat12mois

Il y a 1513 élevages dans la base de données répartis comme suit en Loire Atlantique :
```{r, echo=FALSE, include=FALSE}
BVD_LOIRE_ATLANTIQUE <- read.csv2("BVD_LOIRE_ATLANTIQUE.csv")
str(BVD_LOIRE_ATLANTIQUE)
BVD_LOIRE_ATLANTIQUE$DATE.PRELEVEMENT <- as.Date(BVD_LOIRE_ATLANTIQUE$DATE.PRELEVEMENT)
BVD_LOIRE_ATLANTIQUE$NUMERO.EDE <- as.factor(BVD_LOIRE_ATLANTIQUE$NUMERO.EDE)
```

```{r, echo=FALSE, include=FALSE}
######### DENSITE D'ELEVAGE PAR COMMUNE ##########
length(unique(BVD_LOIRE_ATLANTIQUE$NUMERO.EDE))  ### 1513 élevages dans la base de données

## Pour chacune des communes on va calculer le nombre d'élevages qui s'y trouvent
elevage <- read.csv2("elevage.csv")

### Cartographie des elevages
pathToShp <- "~/stocfree-model/05_riskFactors/Pays de la Loire/pays-de-la-loire"
ogrInfo(dsn = pathToShp, layer="pays-de-la-loire")
Pays.de.la.Loire <- readOGR(dsn = pathToShp, layer="pays-de-la-loire", stringsAsFactors=FALSE)
Loire.Atlantique <- Pays.de.la.Loire[Pays.de.la.Loire$CODE_DEPT=="44",]
Loire.Atlantique@data <- left_join(Loire.Atlantique@data, elevage, by = c("NOM_COMM" = "COMMUNE2"))
head(Loire.Atlantique@data)
classPrev <- classIntervals(Loire.Atlantique@data$nombre_elevage, 6, style = "fixed", 
                            fixedBreaks=c(0,5,10,15,20,30,100))
palette <- brewer.pal(n = 6, name="YlOrRd")
Loire.Atlantique@data$nombre_elevage <- as.character(cut(Loire.Atlantique@data$nombre_elevage, breaks = classPrev$brks, labels = palette, include.lowest = TRUE))
legende <- as.character(levels(cut(elevage$nombre_elevage, breaks = classPrev$brks, include.lowest = TRUE, right = FALSE)))
str(Loire.Atlantique@data)
```

```{r, echo=FALSE}
plot(Loire.Atlantique, col=Loire.Atlantique@data$nombre_elevage, border="black")
legend("bottomleft", legend = legende, fill = palette, cex=0.7, title = "Nombre d'?levage")
title(main = "Densit? des ?levages en Loire Atlantique", cex.sub = 0.7)

```

- On peut voir que le nombre de test à augmenté au cours du temps
- On également voir que les dates de prélevement on changé au cours du temps, par exemple les prélèvements au printemps 2002 étaient plutôt fait en janvier alors qu'en 2019 c'était en février
```{r, echo=FALSE,include=FALSE}
## Nombre de test réalisés a chaque campagne
table((BVD_LOIRE_ATLANTIQUE$CAMPAGNE))

## Moyenne des dates de prélevements pour quelques périodes
mean(BVD_LOIRE_ATLANTIQUE$DATE.PRELEVEMENT[BVD_LOIRE_ATLANTIQUE$CAMPAGNE=="PRINTEMPS 2002"])
mean(BVD_LOIRE_ATLANTIQUE$DATE.PRELEVEMENT[BVD_LOIRE_ATLANTIQUE$CAMPAGNE=="PRINTEMPS 2005"])
mean(BVD_LOIRE_ATLANTIQUE$DATE.PRELEVEMENT[BVD_LOIRE_ATLANTIQUE$CAMPAGNE=="PRINTEMPS 2010"])
mean(BVD_LOIRE_ATLANTIQUE$DATE.PRELEVEMENT[BVD_LOIRE_ATLANTIQUE$CAMPAGNE=="PRINTEMPS 2014"])
mean(BVD_LOIRE_ATLANTIQUE$DATE.PRELEVEMENT[BVD_LOIRE_ATLANTIQUE$CAMPAGNE=="PRINTEMPS 2019"])


## Nombre de vendeurs = 9473
length(unique(BVD_LOIRE_ATLANTIQUE$N.EDE.VENDEUR))

```


### Variables quantitatives : TAUX, NOTE, NB.ACHAT, NB.ACHAT.6.MOIS, NB.ACHAT.12.MOIS

```{r, echo=FALSE,include=FALSE}
summary(BVD_LOIRE_ATLANTIQUE$TAUX, na.rm=T)
sd(BVD_LOIRE_ATLANTIQUE$TAUX, na.rm=T)
```

```{r, echo=FALSE}
hist(BVD_LOIRE_ATLANTIQUE$TAUX, col=rainbow(130), main="Répartition du taux de l'ELISA réalisé sur lait de Tank", xlab="Valeurs du taux", ylab="Fréquence")

tab1(BVD_LOIRE_ATLANTIQUE$NOTE[BVD_LOIRE_ATLANTIQUE$CAMPAGNE=="PRINTEMPS 2019"], main="Nombre de non infecté et d'infecté au Printemps 2019", ylab="Fréquence", col=rainbow(10),xlab=c("Non infecté","Infecté")) # 26% de note 1 et 74% de 0 sur 9 ans 

tab1(BVD_LOIRE_ATLANTIQUE$NOTE[BVD_LOIRE_ATLANTIQUE$CAMPAGNE=="PRINTEMPS 2017"], main="Nombre de non infecté et d'infecté au Printemps 2017", ylab="Fréquence", col=rainbow(10),xlab=c("Non infecté","Infecté"))

```

## Analyse bivariée

```{r, echo=FALSE}
plot(BVD_LOIRE_ATLANTIQUE$TAUX, BVD_LOIRE_ATLANTIQUE$NOTE, main="Distribution du taux de l'ELISA en fonction de la variable binaire note", xlab="Taux", ylab="Note", col="green")

plot(BVD_LOIRE_ATLANTIQUE$TAUX, BVD_LOIRE_ATLANTIQUE$CAMPAGNE, main="Distribution du taux de l'ELISA en fonction de la période", xlab="Taux", ylab="Période", col="green")

boxplot(TAUX ~ achat, data = BVD_LOIRE_ATLANTIQUE)
boxplot(TAUX ~ achat6mois, data = BVD_LOIRE_ATLANTIQUE)
boxplot(TAUX ~ achat12mois, data = BVD_LOIRE_ATLANTIQUE)
pirateplot(TAUX ~ achat6mois, data = BVD_LOIRE_ATLANTIQUE, theme = 1, inf.method = "ci", bar.f.o = 0.1, bar.f.col = "grey10")

```

```{r, echo=FALSE}

t.test(BVD_LOIRE_ATLANTIQUE$TAUX[BVD_LOIRE_ATLANTIQUE$achat=="Oui"],BVD_LOIRE_ATLANTIQUE$TAUX[BVD_LOIRE_ATLANTIQUE$achat=="Non"], alternative="greater")
## Au seuil alpha=5%, il semble que le taux dans les élevages ou il y a eu des achats soit plus elevé que dans celui ou il n'y en a pas eu.

t.test(BVD_LOIRE_ATLANTIQUE$TAUX[BVD_LOIRE_ATLANTIQUE$achat6mois=="Oui"],BVD_LOIRE_ATLANTIQUE$TAUX[BVD_LOIRE_ATLANTIQUE$achat6mois=="Non"], alternative="greater")
## Au seuil alpha=5%, il semble que le taux dans les élevages ou il y a eu des achats dans les 6 derniers mois soit plus elevé que dans celui ou il n'y en a pas eu.

t.test(BVD_LOIRE_ATLANTIQUE$TAUX[BVD_LOIRE_ATLANTIQUE$achat12mois=="Oui"],BVD_LOIRE_ATLANTIQUE$TAUX[BVD_LOIRE_ATLANTIQUE$achat12mois=="Non"], alternative="greater")
## Au seuil alpha=5%, il semble que le taux dans les élevages ou il y a eu des achats dans les 12 derniers mois soit plus elevé que dans celui ou il n'y en a pas eu.

reg <- lm(TAUX ~ achat, data = BVD_LOIRE_ATLANTIQUE)
reg <- lm(TAUX ~ achat6mois, data = BVD_LOIRE_ATLANTIQUE)
reg <- lm(TAUX ~ achat12mois, data = BVD_LOIRE_ATLANTIQUE)
summary(reg)

```


### Variables qualitatives : NUMERO.EDE, CP2, COMMUNE2, CANTON, CAMPAGNE, DATE.PRELEVEMENT, N.EDE.VENDEUR, NOTE, type_elevage
### achat, achat6mois, achat12mois, DATE_ACHAT, infection_voisinage, nouvelle_infection

```{r}
tab1(BVD_LOIRE_ATLANTIQUE$NOTE)

tab1(BVD_LOIRE_ATLANTIQUE$CAMPAGNE)
table(BVD_LOIRE_ATLANTIQUE$COMMUNE2, exclude=T)
tab1(BVD_LOIRE_ATLANTIQUE$COMMUNE2)

length(unique(BVD_LOIRE_ATLANTIQUE$N.EDE.VENDEUR))

tab1(BVD_LOIRE_ATLANTIQUE$type_elevage)

tab1(BVD_LOIRE_ATLANTIQUE$achat)
tab1(BVD_LOIRE_ATLANTIQUE$achat6mois)
tab1(BVD_LOIRE_ATLANTIQUE$classe_NB.achat6mois)
tab1(BVD_LOIRE_ATLANTIQUE$achat12mois)
tab1(BVD_LOIRE_ATLANTIQUE$classe_NB.achat12mois)
tab1(BVD_LOIRE_ATLANTIQUE$Infection_voisinage)
tab1(BVD_LOIRE_ATLANTIQUE$classe_infection_voisinage)
tab1(BVD_LOIRE_ATLANTIQUE$nouvelle_infection)
summary(BVD_LOIRE_ATLANTIQUE$Voisinage)
```
### Variables quantitatives : TAUX, Nombre_bovin, NB.ACHAT.6.MOIS, NB.ACHAT.12.MOIS, Voisinage, nombre_elevage
```{r}
summary(BVD_LOIRE_ATLANTIQUE$TAUX)
sd(BVD_LOIRE_ATLANTIQUE$TAUX, na.rm=T)

summary(BVD_LOIRE_ATLANTIQUE$nombre_bovin)
sd(BVD_LOIRE_ATLANTIQUE$nombre_bovin, na.rm=T)

summary(BVD_LOIRE_ATLANTIQUE$NB.ACHAT.6.MOIS)
sd(BVD_LOIRE_ATLANTIQUE$NB.ACHAT.6.MOIS, na.rm=T)

summary(BVD_LOIRE_ATLANTIQUE$NB.ACHAT.12.MOIS)
sd(BVD_LOIRE_ATLANTIQUE$NB.ACHAT.12.MOIS, na.rm=T)

summary(BVD_LOIRE_ATLANTIQUE$Voisinage)
sd(BVD_LOIRE_ATLANTIQUE$Voisinage, na.rm=T)

summary(BVD_LOIRE_ATLANTIQUE$nombre_elevage)
sd(BVD_LOIRE_ATLANTIQUE$nombre_elevage, na.rm=T)

summary(BVD_LOIRE_ATLANTIQUE$taille_elevage_origine)
sd(BVD_LOIRE_ATLANTIQUE$taille_elevage_origine, na.rm=T)
```

