---
title: "Simulation of data for the evaluation of the STOC free model"
output: 
  html_notebook:
    theme: united
    toc: yes
  html_document:
    toc: yes
---

```{r first, echo = FALSE, message=FALSE, warning=FALSE}
## Libraries
library(DiagrammeR)
```

# Introduction

The aim of this document is to describe a function that simulates herd data for checking and evaluating the STOC free model. The simulation model uses the same hypotheses as the STOC free model described in the *model description* directory.


# Simulation model

The presence/absence of an infection is simulated at the herd level at regular time steps. Between each time step, uninfected herds have a certain probability of getting infected and infected herds have a certain probability of not eliminating the infection.

```{r, echo = FALSE}
grViz("

digraph model {

graph [layout = neato]

node [shape = rectangle]

I [pos = '0,1.5!', shape = circle]
S [pos = '0,0!', shape = circle]

I -> S [headlabel = <1-&#964;<SUB>2</SUB>>, labeldistance=3.5]
S -> I [headlabel = '', labeldistance=2]

tau1Box [pos = '-.2,.75!', label = <&#964;<SUB>1</SUB>>, width = .1, penwidth = 0]

F [pos = '-1.75, .75!', label = 'F', penwidth = 0, width = .1]

T1 [pos = '1.5,0!', label = <T<SUP>-</SUP>>, penwidth = 0, width = .1]
T2 [pos = '1.5,1.5!', label = <T<SUP>+</SUP>>, penwidth = 0, width = .1]

S -> T1 [label = Sp]; I -> T2 [label = Se]

F -> tau1Box [label = <&#946;>]

}

")
```

Regarding the presence of infection, 2 states are considered :

- **S** for susceptible
- **I** for infected

The transition between these states is simulated

- $\tau_1$ is the probability of becoming infected between two consecutive time points
- $\tau_2$ is the probability of remaining infected between two consecutive time points. Therefore, the probability of transitioning from I to S is $1-\tau_2$

A test is performed to determine whether the infection is present.

- **Se**, the sensitivty, is the probability for an infected herd to get a positive test result.
- **Sp**, the specificity, is the probability for an uninfected herd to get a negative test result.


# Parameters used in simulation

The variable names used in the function need to be easy to understand.

## nHerds: number of herds

This is the number of herds used in the simulation.

## nTests: number of tests per herd

This is the number of tests performed per herd. It is assumed to be the same for all herds.

## prev: infection prevalence

This is the prevalence of infection in the population. The prevalence is constrained by the infection dynamics i.e. $\tau_1$ and $\tau_2$. In the function, the prevalence and $\tau_1$ are set at chosen values and $\tau_2$ is calculated from them.

## Se: test sensitivity

Sensitivity of the test, i.e. the probability of getting a positive test when the infection is present in a herd.

## Sp: test specificity

Specificity of the test, i.e. the probability of getting a negative test result when the infection is not present in a herd.

## tau1: probabilty of new infection

*tau1* is the probability for a herd to become infected when the infection was not present on the previous test. Setting a prevalence and a probability of new infection determines the probability of elimination of infection because at equilibrium (when prevalence is stable), the number of new infections cancels out the number of eliminated infections. This can be written as follows :


$$(1 - \tau_2) \pi = \tau_1 (1 - \pi)$$
$$\pi - \tau_2 \pi  = \tau_1 - \tau_1 \pi$$
$$\tau_2 \pi  = \pi - \tau_1 + \tau_1 \pi$$
$$\tau_2   = \frac{1}{\pi} (\pi - \tau_1 + \tau_1 \pi)$$
$$\tau_2   =  1 + \tau_1 - \frac{\tau_1}{\pi} $$

where $\pi$ is the prevalence at equilbrium.

The function below computes *tau2* from *tau1* and *prev*

```{r}
tau2f <- function(prev, tau1) 1 + tau1 - tau1 / prev
```

However, simulating *tau2* values using this function can produce negative probabilities which is impossible.

```{r}
tau1i <- seq(.1, .9, by = .1)
curve(tau2f(x, tau1i[1]), from = 0, to = 1,
      ylab = expression(tau[2]),
      xlab = expression(pi))

for(i in 2:length(tau1i)){

   curve(tau2f(prev = x, tau1 = tau1i[i]), from = 0.01, to = .99, add = TRUE, col = i)

  
}
```

The function is modified to prevent the generation of negative tau2 values

```{r}
tau2F <- function(prev, tau1){
  
  tau2 <- 1 + tau1 - tau1 / prev
  
  if(tau2 > 0) return(tau2) else return(NA)

  }
```

It now behaves as expected.

```{r}
tau1i <- seq(.1, .9, by = .1)
xx <- seq(0, 1, by = .01)
plot(xx, sapply(xx, function(x) tau2F(prev = x, tau1i[1])), type = "l",
     ylim = c(0, 1),
     ylab = expression(tau[2]),
     xlab = expression(pi))

for(i in 2:length(tau1i)){

   lines(xx, sapply(xx, function(x) tau2F(prev = x, tau1i[i])), col = i)
 }
```


```{r, echo = FALSE}
rm(list= c("tau1i", "xx"))
```

## rfF and rfRR: risk factor frequency and relative risk of infection

In the function, the probabilities of new infection ($\tau_1$), the frequency of occurrence of a risk factor($F$) and the relative risk associated with the presence of this risk factor ($\gamma$) are used as inputs. From this, the probability of infection in herds not exposed to the risk factor in the population ($\alpha$) is calculated.

$$\tau_1 = (1 - F) \alpha + F \alpha \gamma$$
$$\tau_1 = \alpha ((1 - F) + F \gamma)$$
$$\alpha = \frac{\tau1}{1 - F + F \gamma}$$

In the function:

- $F$ is named **rfF**
- $\gamma$ is named **rfRR**

**alpha** is calculated using the function below

```{r}
alphaF <- function(tau1, rfF, rfRR){ tau1 / (1 - rfF + rfF * rfRR) }
```


# Simulation function

The function below simulates infection dynamics over *nTests* in *nHerds*.

```{r}
HerdInfSim <- function(nHerds, nTests, prev, Se, Sp, tau1, rfF, rfRR){

  # tau2
  tau2 <- tau2F(prev = prev, tau1 = tau1)
  
  # bet1
  alpha <- alphaF(tau1 = tau1, rfF = rfF, rfRR = rfRR)
  
  # Creation of the simulated dataset 
  HerdInf <- data.frame(
    herdID = rep(1:nHerds, each = nTests),  
    test_t = rep(1:nTests, nHerds), 
    status = rep(NA),  
    testRes = rep(NA),
    rf = rbinom((nHerds * nTests), 1, rfF), 
    pi = rep(NA) 
  )
  
  # Simulation of probability of infection
  
  # probability of infection at time 1 is 
  HerdInf$pi[HerdInf$test_t == 1] <- prev
  HerdInf$status[HerdInf$test_t == 1] <- rbinom(nHerds, 1 ,prev) 
  
  for (k in 2:(nTests)) {
    
    # rows for which infection and risk factor are absent on the previous test
    kprev_rf0 <- which(HerdInf$test_t == (k-1) & HerdInf$status == 0 & HerdInf$rf == 0)
    HerdInf$pi[kprev_rf0 + 1] <- alpha
    # rows for which infection is absent and risk factor is present on the previous test
    kprev_rf1 <- which(HerdInf$test_t == (k-1) & HerdInf$status == 0 & HerdInf$rf == 1)
    HerdInf$pi[kprev_rf1 + 1] <- alpha * rfRR
    # rows for which the infection is present on the previous test
    kprev_inf1 <- which(HerdInf$test_t == (k-1) & HerdInf$status == 1)
    HerdInf$pi[kprev_inf1 + 1] <- tau2
    
    ## Simulation of the true status for all herds at time k
    HerdInf$status[HerdInf$test_t == k] <- rbinom(nHerds, 1, HerdInf$pi[HerdInf$test_t == k])
    
  }
  
  ## Simulation of test result from true status
  ### when the true status is 'infected'
  ipos <- which(HerdInf$status == 1)
  npos <- length(ipos)
  HerdInf$testRes[ipos] <- rbinom(npos, 1, Se)
  
  
  ### when the true status is 'not infected'
  ineg <- which(HerdInf$status == 0)
  nneg <- length(ineg)
  HerdInf$testRes[ineg] <- rbinom(nneg, 1, 1 - Sp)
  
  HerdInf
  
  }
```

# Function for calculating the observed values of the parameters used in the simulation

```{r}
herdInfCheck <- function(herdInf){

  # Previous value for status
  herdInf$status_p <- c(NA, herdInf$status[-nrow(herdInf)])
  herdInf$status_p[herdInf$test_t == 1] <- NA
  # Previous value for presence of risk factor
  herdInf$rf_p <- c(NA, herdInf$rf[-nrow(herdInf)])
  herdInf$rf_p[herdInf$test_t == 1] <- NA

  # Number of herds  
  nHerdsObs <- length(unique(herdInf$herdID))
  # Number of tests
  NTestMinObs <- min(with(herdInf, tapply(test_t, herdID, min)))
  NTestMaxObs <- max(with(herdInf, tapply(test_t, herdID, max)))
  # Prevalence
  prevObs <- sum(herdInf$status) / nrow(herdInf)
  # Sensitivity
  SeObs <- sum(herdInf$testRes[herdInf$status == 1]) / nrow(herdInf[herdInf$status == 1,])
  # Specificity
  SpObs <- length(herdInf$testRes[herdInf$testRes == 0 & herdInf$status == 0]) / nrow(herdInf[herdInf$status == 0,])

  
  # Status at previous time
  tabStatusPrev <- with(herdInf, table(status_p, status))

  # tau1
  tau1Obs <- tabStatusPrev[1, 2] / sum(tabStatusPrev[1,])
  # tau2
  tau2Obs <- tabStatusPrev[2, 2] / sum(tabStatusPrev[2,])
  
  ## rfF
  rfFObs <- with(herdInf[herdInf$test_t > 1,],
             sum(rf) / length(rf))
  
  # rfRRobs
  rfRRObs <- with(herdInf[herdInf$test_t > 1,], 
     (length(which(status == 1 & rf_p == 1 & status_p == 0)) / length(which(rf_p == 1 & status_p == 0))) /
     (length(which(status == 1 & rf_p == 0 & status_p == 0)) / length(which(rf_p == 0 & status_p == 0))) )

  simCheck <- round(
               c(nHerdsObs,
                NTestMinObs,
                NTestMaxObs,
                prevObs,
                SeObs,
                SpObs,
                tau1Obs,
                tau2Obs,
                rfFObs,
                rfRRObs), 3)
  
  names(simCheck) <- c("nHerdsObs", "NTestMinObs", "NTestMaxObs", "prevObs",
                       "SeObs", "SpObs", "tau1Obs", "tau2Obs", "rfFObs", "rfRRObs")
  
  simCheck
  
}
```

# An example

The data generating function is tested. Data are generated for 100 herds over 12 tests with the parameters below.

```{r}
herdInf <- HerdInfSim(
               nHerds = 100, 
               nTests = 12, 
               prev = .5, 
               Se = .8, 
               Sp = .9, 
               tau1 = .1, 
               rfF = .3,  
               rfRR = 2)
```

Value calculated for tau2

```{r}
tau2F(prev = .5, tau1 = .1)
```

Value calculated for alpha 

```{r}
alphaF(tau1 = .1, rfF = .3, rfRR = 2)
```

Checking whether the generated data are consistent with some of the parameters used in the simulation.

```{r}
herdInfCheck(herdInf)
```